; =============================== DATA SECTION =================================
SECTION "STDIO_DATA", ROMX

; ==============================================================================
; Font tiles
; ==============================================================================
FontTiles:
INCBIN "assets/font.bin"
FontTilesEnd:

; ==============================================================================
; Allowed characters. The character '_' is used to break lines
; ==============================================================================
AllowedCharacters:
    db      " 0123456789.,:;'!?+-/*ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqr"
    db      "stuvwxyz$_"
AllowedCharactersEnd:

; ==============================================================================
; Mapped allowed caracters
; ==============================================================================
FontMap:
    db      $80, $81, $82, $83, $84, $85, $86, $87, $88, $89, $8A, $8B, $8C, $8D
    db      $8E, $8F, $90, $91, $92, $93, $94, $95, $96, $97, $98, $99, $9A, $9B
    db      $9C, $9D, $9E, $9F, $A0, $A1, $A2, $A3, $A4, $A5, $A6, $A7, $A8, $A9
    db      $AA, $AB, $AC, $AD, $AE, $AF, $B0, $B1, $B2, $B3, $B4, $B5, $B6, $B7
    db      $B8, $B9, $BA, $BB, $BC, $BD, $BE, $BF, $C0, $C1, $C2, $C3, $C4, $C5
    db      $C6, $C7, $C8, $C9, $CA, $CB
FontMapEnd:

; ============================== LOGIC SECTION =================================
SECTION "STDIO_LOGIC", ROM0

; ==============================================================================
; Initialize the font in the VRAM
;
; Requires: HL, DE, BC
; ==============================================================================
InitFont:
    ld      hl, $8800                   ; Set the writing memory address
    ld      de, FontTiles               ; Load the font tiles
    ld      bc, FontTilesEnd - FontTiles ; Load the font tiles length

.copyFont
    ld      a, [de]                     ; Load the font tile
    ld      [hli], a                    ; Place it at the destination, incremen-
                                        ; ting HL
    inc de                              ; Move to next tile
    dec bc                              ; Decrement count
    ld a, b                             ; Check if count is 0...
    or c
    jr nz, .copyFont                    ; ... if it's not, continue
    ret                                 ; Else, return

; ==============================================================================
; Writes a string loaded in DE at the position HL in an area B x C.
;
; HL - Start writing position.
; DE - String to print.
; B - Width of the area (0 ~ 1F).
; C - Height of the area (0 ~ 1F).
; ==============================================================================
Print:
    push    hl                          ; Save the writing start position
    push    bc                          ; Save the writing box width and hight

; Start subroutine
.printChar
    ld      a, [de]                     ; Load character at address [de].
    inc     de                          ; Next character
    cp      0                           ; If I reach the end of the line (char-
    ret     z                           ; acter '\0', return

    cp      $5F                         ; If the character is '_', a breakline 
    push    af                          ; is added and move to the next charac-
    call z, BreakLineChar               ; ter
    pop     af
    jr z,   .printChar


    push    bc                          ; Save BC, DE ed HL
    push    de                          
    push    hl                          
    call    GetChar                     ; Get the character map value
    pop     hl                          ; Restore HL, DE e BC
    pop     de                          
    pop     bc

    push    af                          ; Save AF, BC and DE
    push    bc
    push    de
    call    WaitChar                    ; Wait to write the character
    pop     de                          ; Restore DE and BC
    pop     bc
    
; Wait V-Blank
.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop     af                          ; Restore AF

    ld      [hli], a                    ; Write the character at the destination

    dec     b                           ; Decrement the line counter
    ld      a, b                        ; Check if the counter is 0
    cp      0
    jr nz,  .printChar                  ; If the counter is not 0, print next
                                        ; character
    pop     bc                          ; Save BC and HL
    pop     hl
    call z, BreakLine                   ; If counter is 0, add a break line
    push    hl                          ; Restore HL
    dec     c                           ; Decrement the row counter
    push    bc                          ; Save BC

    ld      a, c                        ; Check if row counter is 0
    cp      0
    jr z,   .exit                       ; Return if row counter is 0

    jr      .printChar                  ; Move to next character

.exit
    pop bc
    pop hl

    ret

; ==============================================================================
; Break line
; 
; HL - Original writing position
; ==============================================================================
BreakLine:
    push    de                          ; Save DE
    ld      de, $0020                   ; Load the break line length
    add     hl, de                      ; Add the break line
    pop     de                          ; Restore DE
    ret                                 ; Return

; ==============================================================================
; Break line with character '_' is found
; ==============================================================================
BreakLineChar:
    add     sp, 4                       ; Move the stack pointer to the "Print"
                                        ; level
    pop     bc                          ; Restore BC
    dec     c                           ; Decrement the row counter
    pop     hl                          ; Restore HL
    call    BreakLine                   ; Add break line
    push    hl                          ; Restore Hl and BC
    push    bc
    add     sp, -4                      ; Move the stack pointer to the call
                                        ; address
    ret                                 ; Return

; ==============================================================================
; Return the mapped value of the character in A
;
; A - Character to map
;
; Return: A
; ==============================================================================
GetChar:
    ld      de, AllowedCharacters       ; Load the allowed characters
    ld      bc, AllowedCharactersEnd - AllowedCharacters ; Set the allowed cha- 
                                        ; racters length
    ld      l, a                        ; Save the character to convert

; Search for the character in the allowed character string
.searchForChar
    ld      a, [de]                     ; Load the allowed character

    cp      l                           ; Check if is the character to convert
    push    hl                          ; Save HL, DE and AF
    push    de                     
    push    af
    call z, GetMapValue                 ; If is the character to convert, get
                                        ; the map value
    pop     af                          ; Restore AF
    ld      a, d                        ; Set the character mapped value
    pop     de                          ; Restore DE and HL
    pop     hl                      
    ret z                               ; If the character is converted, return

    ld      a, b                        ; Check if counter is 0
    or      c
    call z, GetUnknownChar              ; If so, load unknown character and 
    ret z                               ; return

    inc     de                          ; Move to next character
    dec     bc                          ; Decrement the counter
    jr nz,  .searchForChar              ; Check next character

; ==============================================================================
; Returns the mapped value of the character
;
; BC - Map index
;
; Returns: A
; ==============================================================================
GetMapValue:
    ld      de, AllowedCharactersEnd - AllowedCharacters ; Set the map legth

    ; Get the exact map index value
    ld      a, e                        ; Since the map length can be contained
                                        ; in a byte, load the LS byte
    sub     c                           ; Subtract the map index
    ld      c, a                        ; Restore the updated value

    ; Get the mapped value
    ld      hl, FontMap                 ; Load the font map in HL
    add     hl, bc                      ; Add the calculated map index
    ld      d, [hl]                     ; Load the value at index [hl] in A
    ret                                 ; Return

; ==============================================================================
; Load in A the unknown character '?'
;
; Returns: A
; ==============================================================================
GetUnknownChar:
    ld      a, $91                      ; Load in A the character '?'
    ret                                 ; Return

; ==============================================================================
; Char by char printing timer
;
; Requires: A, BC
; ==============================================================================
WaitChar:
    ld      bc, $2000                   ; Set the timer at $2000

.startWait
    dec     bc                          ; Decrement the timer
    ld      a, b                        ; Check if the timer is 0
    or      c
    jr      nz, .startWait              ; If it's not, loop
    ret z                               ; Else, return
