INCLUDE "inc/hardware.inc"
INCLUDE "stdio.asm"
INCLUDE "gui.asm"
INCLUDE "dialogue.asm"

SECTION "Header", ROM0[$100]

EntryPoint:
	di 
	jp Start

REPT $150 - $104
	db 0
ENDR

SECTION "Game code", ROM0

Start:
; .waitVBlank
; 	ld a, [rLY]
; 	cp 144
; 	jr c, .waitVBlank

	xor a
	ld [rLCDC], a

	call InitFont
    call InitGUI

    ; Init display registers
    ld a, %11100100
    ld [rBGP], a

    xor a ; ld a, 0
    ld [rSCY], a
    ld [rSCX], a

    ; Shut sound down
    ld [rNR52], a

    ; Turn screen on, display background
    ld a, %10000001
    ld [rLCDC], a

    ld de, HelloWorldStr
    call StartDialogue

	; Lock up
.lockup
    jr .lockup

SECTION "Hello World string", ROM0

HelloWorldStr:
    db "Hello world!_   -Sidereal Works", 0
