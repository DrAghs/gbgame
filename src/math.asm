SECTION "Math", ROM0

Multiply:
.loop
    add b
    dec b
    ld d, a
    ld a, b
    cp 0
    ld a, d
    ret z
    jr nz, .loop
