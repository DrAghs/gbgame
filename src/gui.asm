; =============================== DATA SECTION =================================
SECTION "GUI_DATA", ROMX

; ==============================================================================
; GUI tiles
; ==============================================================================
GUITiles:
INCBIN "assets/gui_gfx.bin"
GUITilesEnd:

; ==============================================================================
; Corners tile map
; ==============================================================================
CornersTileMap:
    db      $CB                         ; Top-left
    db      $CC                         ; Top-right        
    db      $CD                         ; Bottom-right
    db      $CE                         ; Bottom-left
CornersTileMapEnd:

; ==============================================================================
; Edges tile map
; ==============================================================================
EdgesTileMap:
    db      $CF                         ; Top
    db      $D0                         ; Right
    db      $D1                         ; Bottom
    db      $D2                         ; Right
EdgesTileMapEnd:

; ==============================================================================
; Arrows tile map
; ==============================================================================
ArrowsTileMap:
    db      $D3                         ; Right
    db      $D4                         ; Left
    db      $D5                         ; Up
    db      $D6                         ; Down
ArrowsTileMapEnd:

; ============================== LOGIC SECTION =================================
SECTION "GUI_LOGIC", ROM0

; ==============================================================================
; Initialize the GUI.
;
; Require: HL, DE, BC
; ==============================================================================
InitGUI:
    ld      hl, $8CB0                   ; Set the writing memory address.
    ld      de, GUITiles                ; Load the GUI tiles
    ld      bc, GUITilesEnd - GUITiles  ; Load the GUI tiles length

.copyGUI
    ld a, [de]                          ; Load the GUI tile
    ld [hli], a                         ; Place it at the destination, incremen-
                                        ; ting HL
    inc de                              ; Move to next tile
    dec bc                              ; Decrement count
    ld a, b                             ; Check if count is 0...
    or c
    jr nz, .copyGUI                     ; ... if it's not, continue
    ret                                 ; Else, return

; ==============================================================================
; Draw the GUI box at the position HL with width of B and height of C
;
; HL - Start drawing position
; B - Width of the box (0 ~ 1F)
; C - Height of the box (0 ~ 1F)
; ==============================================================================
DrawGUI:
    push    hl                          ; Save the drawing start position

    ld d, b
    ld e, c

    dec c

    call DrawTopLine
    pop hl
    call NewLine
    push hl

.drawBody
    ld      a, c
    cp 0
    call nz, DrawBody
    pop hl
    call NewLine
    push hl
    jr nz, .drawBody
    ; jr z, .drawBottomLine

.drawBottomLine
    call DrawBottomLine

    pop hl

    ret

; Draw the top line
DrawTopLine:
.drawTopLine
    ld      a, b
    cp      d
    call z, DrawTopLeftCorner
    jr z,   .drawTopLine

    ld      a, b
    cp      1
    call nz, DrawTopEdge
    jr nz,  .drawTopLine

    call z, DrawTopRightCorner
    ret z

DrawTopLeftCorner:
    push af
    dec b
    push de
    ld      de, CornersTileMap
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

DrawTopRightCorner:
    push af
    dec b
    push de
    ld      de, CornersTileMap + 1
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

DrawTopEdge:
    push af
    dec b
    push de
    ld      de, EdgesTileMap
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

DrawBody:
.drawBody
    ld      a, b
    cp      d
    call z, DrawLeftEdge
    jr z,   .drawBody

    ld      a, b
    cp      1
    call nz, DrawEmpty
    jr nz,  .drawBody

    call z, DrawRightEdge
    ret z

DrawLeftEdge:
    push af
    dec b
    push de
    ld      de, EdgesTileMap + 3
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

DrawEmpty:
    inc hl
    dec b
    ret

DrawRightEdge:
    push af
    dec b
    push de
    ld      de, EdgesTileMap + 1
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

DrawBottomLine:
.drawBottomLine
    ld      a, b
    cp      d
    call z, DrawBottomLeftCorner
    jr z,   .drawBottomLine

    ld      a, b
    cp      1
    call nz, DrawBottomEdge
    jr nz,  .drawBottomLine

    call z, DrawBottomRightCorner
    ret z

DrawBottomLeftCorner:
    push af
    dec b
    push de
    ld      de, CornersTileMap + 3
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

DrawBottomRightCorner:
    push af
    dec b
    push de
    ld      de, CornersTileMap + 2
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

DrawBottomEdge:
    push af
    dec b
    push de
    ld      de, EdgesTileMap + 2
    ld      a, [de]
    push    af

.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    pop de

    ld      [hli], a
    pop af
    ret

NewLine:
    push de
    ld de, $0020
    add hl, de
    pop de
    dec c
    ld b, d
    ret
