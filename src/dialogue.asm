SECTION "DIALOGUE_LOGIC", ROM0

; ==============================================================================
; Start a dialogue in a box 20 x 4. Lines must be 18 characters long.
; 
; DE - Dialogue text
; ==============================================================================
StartDialogue:
    ; Draw the GUI
    ld      hl, $99C0                   ; Position the GUI at the bottom
    ld      b, 20                       ; Full width
    ld      c, 4                        ; 4 tiles height (2 lines)
    push de
    call DrawGUI
    pop de

.drawText
    ld      hl, $99E1
    ld      b, 18
    ld      c, 2
    call Print
    call StopText

    ret

StopText:
    ld      hl, $9A33
    ld      a, [ArrowsTileMap + 3]
    push af
.waitVblank                             
    ld      a, [rLY]
	cp      144
	jr c,   .waitVblank
    pop af
    ld      [hl], a

    ret
